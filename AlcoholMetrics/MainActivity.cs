﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using System.IO;
using Android.Util;
using System.Collections.Generic;

namespace AlcoholMetrics
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        // region_below dose counter View.vars
        TextView doseCounter;
        int counterVar; //var var 
        bool doseSelected;

        // region_below choiceVars
        string quantityText;
        string concText;
        int doseID;


        // region_below listView vars
        List<string> activeDoses;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            activeDoses = new List<string>();
            doseCounter = FindViewById<TextView>(Resource.Id.doseCounter);

            Button incrementButton = FindViewById<Button>(Resource.Id.incrementButton);
            Button decrementButton = FindViewById<Button>(Resource.Id.decrementButton);
            Button selectDoseParametersButton = FindViewById<Button>(Resource.Id.selectDoseParametersButton);
            ListView listView = FindViewById<ListView>(Resource.Id.listView1);

            ArrayAdapter<string> lstAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, activeDoses);
            listView.Adapter = lstAdapter;


            //5:07
            counterVar = ReadCounterData("storage_interface.txt");
            UpdateCounter();
            ReadActiveDosesData("active_doses.txt");
            lstAdapter.Clear();
            lstAdapter.AddAll(activeDoses);

            //5:16
            doseSelected = false;
            doseID = -1;

            incrementButton.Click += delegate
            {
                HandleIncrement();
                lstAdapter.Clear();
                lstAdapter.AddAll(activeDoses);
            };

            decrementButton.Click += delegate
            {
                HandleDecrement();
                lstAdapter.Clear();
                lstAdapter.AddAll(activeDoses);

            };

            selectDoseParametersButton.Click += delegate
            {
                LayoutInflater layoutInflaterAndroid = LayoutInflater.From(this);
                View mView = layoutInflaterAndroid.Inflate(Resource.Layout.alcoholic_substance_uinput_dialog, null);
                Android.Support.V7.App.AlertDialog.Builder alertDialogBuilder = new Android.Support.V7.App.AlertDialog.Builder(this);
                alertDialogBuilder.SetView(mView);

                Android.Support.V7.App.AlertDialog alertDialogAndroid = alertDialogBuilder.Create();

                // 17 40
                var sw1 = mView.FindViewById<Android.Widget.ToggleButton>(Resource.Id.toggleButton1);
                var sw2 = mView.FindViewById<Android.Widget.ToggleButton>(Resource.Id.toggleButton2);
                var bt1 = mView.FindViewById<Android.Widget.Button>(Resource.Id.confirmButton);
                var lb1 = mView.FindViewById<Android.Widget.TextView>(Resource.Id.textView123);


                var quantityEditText = mView.FindViewById<Android.Widget.EditText>(Resource.Id.editTextAreaQuantity);
                var concEditText = mView.FindViewById<Android.Widget.EditText>(Resource.Id.editTextAreaConc);

                if (concText != "")
                    concEditText.Text = concText;
                if (quantityText != "")
                    quantityEditText.Text = quantityText;

                sw1.Click += delegate
                {
                    if (sw2.Checked)
                        sw2.Toggle();
                    doseID = 1;
                };

                sw2.Click += delegate
                {
                    if (sw1.Checked)
                        sw1.Toggle();
                    doseID = 2;
                };

                bt1.Click += delegate
                {
                    if (sw1.Checked == false && sw2.Checked == false)
                    {
                        doseID = -1;
                        quantityText = "";
                        concText = "";
                        lb1.SetText("Please configure dose parameters.", null);
                        lb1.SetTextColor(Android.Graphics.Color.Red);
                        lb1.Visibility = Android.Views.ViewStates.Visible;
                        return;
                    }

                    if (concEditText.Text == "" || quantityEditText.Text == "")
                    {
                        doseID = -1;
                        quantityText = "";
                        concText = "";
                        lb1.SetText("Please configure concentration and quantity.", null);
                        lb1.SetTextColor(Android.Graphics.Color.Red);
                        lb1.Visibility = Android.Views.ViewStates.Visible;
                        return;
                    }

                    doseSelected = true;
                    quantityText = quantityEditText.Text;
                    concText = concEditText.Text;
                    
                    lb1.Visibility = Android.Views.ViewStates.Invisible;
                    alertDialogAndroid.Dismiss();
                };

                if (doseID == 1)
                    sw1.Toggle();
                if (doseID == 2)
                    sw2.Toggle();

                if (doseSelected == true && quantityText != "" && concText != "")
                {
                    lb1.SetText("Warning: Dose parameters already configured.", null);
                    lb1.SetTextColor(Android.Graphics.Color.Blue);
                    lb1.Visibility = Android.Views.ViewStates.Visible;
                }

                alertDialogAndroid.Show();
            };
        }

        public void SaveActiveDosesData(string filename)
        {
            var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename);
            using (var writer = File.CreateText(backingFile))
            {
                for (int i = 0; i < activeDoses.Count; ++i)
                {
                    var doseString = activeDoses[i];
                    writer.WriteLine(doseString);
                }
            }
        }

        public void ReadActiveDosesData(string filename)
        {
            var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename);
            if (backingFile == null || !File.Exists(backingFile))
            {
                return;
            }

            using (var reader = new StreamReader(backingFile, true))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    activeDoses.Add(line);
                }
            }
        }

        public int ReadCounterData(string filename)
        {
            // 5:08
            var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename);
            if (backingFile == null || !File.Exists(backingFile))
            {
                Log.Info("app", "returned 0");
                return 0;
            }

            var count = 0;
            using (var reader = new StreamReader(backingFile, true))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (int.TryParse(line, out var newcount))
                    {
                        count = newcount;
                    }
                }
            }

            return count;
        }

        public void SaveCounterData(string filename, int value)
        {
            var backingFile = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), filename);
            using (var writer = File.CreateText(backingFile))
            {
                writer.WriteLine(value);
            }
        }

        public void UpdateCounter()
        {
            doseCounter.Text = counterVar.ToString();
        }

        public void HandleIncrement()
        {
            if (!doseSelected)
                return;

            counterVar += 1;
            doseCounter.Text = counterVar.ToString();
            SaveCounterData("storage_interface.txt", counterVar);

            string formattedString = "";

            if (doseID == 1)
                formattedString += "Beer";
            else if (doseID == 2)
                formattedString += "Shot";

            formattedString += " ";
            formattedString += "Quantity: ";
            formattedString += quantityText;
            formattedString += " ml ";
            formattedString += " ";
            formattedString += "Concentration: ";
            formattedString += concText;
            formattedString += " %vol";


            activeDoses.Add(formattedString);
            SaveActiveDosesData("active_doses.txt");
            
            for (var i = 0; i < activeDoses.Count; ++i)
            {
                Log.Info(activeDoses[i], "a");
            }


            doseID = -1;
            concText = "";
            quantityText = "";
            doseSelected = false;
        }
        public void HandleDecrement()
        {
            if (counterVar > 0)
                counterVar -= 1;
            doseCounter.Text = counterVar.ToString();
            SaveCounterData("storage_interface.txt", counterVar);
            SaveActiveDosesData("active_doses.txt");

            if (activeDoses.Count > 0)
            {
                activeDoses.RemoveAt(activeDoses.Count - 1);
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}