## AlcoholMetrics
![Screenshot1](Images/screenshot_01.png)
![Screenshot2](Images/screenshot_02.png)


AlcoholMetrics is a Xamarin.Android (sometimes called Monodroid) application concieved for android devices that works as a log for your substance intake.

"Xamarin.Android (formerly known as Mono for Android), initially developed by Novell and continued by Xamarin, is a proprietary[88] implementation of Mono for Android-based smart-phones." (references on [Wikipedia](https://en.wikipedia.org/wiki/Mono_(software)#Xamarin.Android))

The development on a laptop with i7-4720 CPU, 4GB RAM, GTX 950M GPU, and an SSD, with 1366/768 resolution, is not the most pleasant using the debugger and android emulator, though it is possible. The bad environment was enough to make me give up on development.

Visual studio 2019 is not really optimized for this kind of work, the editing of xml files for layouts is jumpy, the studio had to be restarted a few times due to environment problems, etc. I'd rather try learning Xamarin.Forms next time.
